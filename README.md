# jrp_css_nav

A CSS-only (no JS) pancake navigation, as currently demonstrated on [my website.](https://www.jayrope.com) 

Code bit is here for everyone to find a working navigation w/o Javascript, and without needing to digest numerous coder forums containing incomplete solutions and lengthy threads.

Contains only HTML and CSS, uses UTF-8 charset.
Minimal styling, using 
> <big>&#8801;</big>

(html entity \&#8801;\) to open and
> <big>&#9746;</big>

(html entity \&#9746;\) to close a navigation menu/linklist, rather than using data hungry images. 

<strong>Notes</strong> <br />
• License: This _really tiny bit_ of code is completely copyleft/free, of course! <br /> 
Just to exclude any liability it comes licensed under [GNU AGPL v3](http://www.gnu.org/licenses/agpl-3.0.html). 
_Pointless to license code, which consists only of all-available html/css_...
• Limit: Does _not_ react to clicking outside the navigation area.
• Personal/link back: I am a professional earhead (composer) with a past as a web dev. You are welcome to do so, but aren't required to link back to jayrope.com. 